/*
 * iXPAD is a simple text editor with bookmark, syntax highlighting, recent activity, spell check
 * 
 * Copyright (C) 2019  Abrar
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package iX.Windows;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

/**
 * @author abrar
 *
 */
public class iXSearch extends JDialog implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel labelFind;
	private JLabel labelReplace;
	private JTextField txtFind;
	private JTextField txtReplace;
	private JCheckBox checkMatchCase;
	private JButton buttonFind;
	private JButton buttonFindNext;
	private JButton buttonReplace;
	private JButton buttonReplaceAll;
	private JButton buttonCancel;
	
	private JTextPane texteditor;

	public iXSearch(JTextPane textEditor) {		
		this.texteditor = textEditor;
		
		setupUI();
		setVisible(true);
	}

	private void setupUI() {
		// Set frame properties
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setMinimumSize(new Dimension(450, 120));
		this.setTitle("iXPAD - Find/Replace");
		this.setLocationRelativeTo(texteditor);
		// ====================
		
		GridLayout mainLayout = new GridLayout(3, 4, 2, 2);
		
		labelFind = new JLabel("Find");
		labelReplace = new JLabel("Replace");
		txtFind = new JTextField();
		txtReplace = new JTextField();
		buttonFind = new JButton("Find");
		buttonFindNext = new JButton("Find Next");
		buttonReplace = new JButton("Replace");
		buttonReplaceAll = new JButton("Replace All");
		checkMatchCase = new JCheckBox("Match Case");
		buttonCancel = new JButton("Cancel");
		
		buttonFind.addActionListener(this);
		buttonFindNext.addActionListener(this);
		buttonReplace.addActionListener(this);
		buttonReplaceAll.addActionListener(this);
		buttonCancel.addActionListener(this);
		
		this.setLayout(mainLayout);
		
		this.add(labelFind, 0);
		this.add(txtFind, 1);
		this.add(buttonFind, 2);
		this.add(buttonFindNext, 3);
		this.add(labelReplace, 4);
		this.add(txtReplace, 5);
		this.add(buttonReplace, 6);
		this.add(buttonReplaceAll, 7);
		this.add(new JLabel(""), 8);
		this.add(new JLabel(""), 9);
		this.add(checkMatchCase, 10);
		this.add(buttonCancel, 11);		
	}
	
	private void findString(String str, int pos) {
		if (str.isEmpty()) {
			return;
		}
		
		if (pos == -1) {
			pos = 0;
		}
		
		String from = texteditor.getText();
		int startPos = -1;
		if (checkMatchCase.isSelected()) {
			startPos = from.indexOf(str, pos);
		} else {
			startPos = from.toLowerCase().indexOf(str.toLowerCase(), pos);
		}
		
		if (startPos == -1) {
			JOptionPane.showMessageDialog(null, "Could not find \"" + str + "\"!");
			return;
		}
		int endPos = startPos + str.length();
		
		texteditor.select(startPos, endPos);
	}
	
	public void find() {
		texteditor.requestFocus();
		
		int currPos = texteditor.getCaretPosition();
		
		String selStr = texteditor.getSelectedText();
		if (selStr == null || selStr == "") {
			if (currPos == texteditor.getText().length()) {
				currPos = 0;
			}
		} else {
			currPos -= selStr.length();
		}
		
		findString(txtFind.getText(), currPos);
		
		texteditor.requestFocus();
	}

	public void findNext() {
		texteditor.requestFocus();
		
		int currSel = texteditor.getCaretPosition();	
		if (currSel == texteditor.getText().length()) {
			currSel = 0;
		}
		
		findString(txtFind.getText(), currSel);
		
		texteditor.requestFocus();
	}

	public void replace() {
		try {
			find();
			String selStr = texteditor.getSelectedText();
			if (selStr != null || selStr != "") {
				texteditor.replaceSelection(txtReplace.getText());
			}
		} catch (NullPointerException e) {
			System.out.print("Null Pointer Exception: " + e);
		}
	}

	public void replaceAll() {
		if (checkMatchCase.isSelected()) {
			texteditor.setText(texteditor.getText().replaceAll(txtFind.getText(), txtReplace.getText()));
		} else {
			texteditor.setText(texteditor.getText().toLowerCase().replaceAll(txtFind.getText().toLowerCase(), txtReplace.getText()));
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonFind) {
			find();
		} else if (e.getSource() == buttonFindNext) {
			findNext();
		} else if (e.getSource() == buttonReplace) {
			replace();
		} else if (e.getSource() == buttonReplaceAll) {
			replaceAll();
		} else if (e.getSource() == buttonCancel) {
			this.dispose();
		}
	}
	
}